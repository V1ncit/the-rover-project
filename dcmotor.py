from machine import Pin, PWM

# initialize front left motor
fl_motor_pin1 = Pin(14, Pin.OUT)    
fl_motor_pin2 = Pin(27, Pin.OUT)     
fl_motor = DCMotor(fl_motor_pin1, fl_motor_pin2)

# initialize rear left motor
rl_motor_pin1 = Pin(13, Pin.OUT)
rl_motor_pin2 = Pin(32, Pin.OUT)
rl_motor = DCMotor(rl_motor_pin1, rl_motor_pin2)

# initialize front right motor
# fr_motor_pin1 = Pin(??, Pin.OUT)
# fr_motor_pin2 = Pin(??, Pin.OUT)
# fr_motor = DCMotor(fr_motor_pin1, fr_motor_pin2)

# initialize rear right motor
# rr_motor_pin1 = Pin(??, Pin.OUT)
# rr_motor_pin2 = Pin(??, Pin.OUT)
# rr_motor = DCMotor(rr_motor_pin1, rr_motor_pin2)

class DCMotor:      
  def __init__(self, pin1, pin2):
        self.pin1=pin1
        self.pin2=pin2

  def forward(self):
    self.pin1.value(0)
    self.pin2.value(1)
    
  def backwards(self,):
        self.pin1.value(1)
        self.pin2.value(0)

  def stop(self):
    self.pin1.value(0)
    self.pin2.value(0)