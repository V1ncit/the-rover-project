from dcmotor import DCMotor       
from machine import Pin, PWM   
from time import sleep          

# initialyze front left motor
fl_motor_pin1 = Pin(14, Pin.OUT)    
fl_motor_pin2 = Pin(27, Pin.OUT)     
fl_motor = DCMotor(fl_motor_pin1, fl_motor_pin2)

# initialyse rear left motor
rl_motor_pin1 = Pin(13, Pin.OUT)
rl_motor_pin2 = Pin(32, Pin.OUT)
rl_motor = DCMotor(rl_motor_pin1, rl_motor_pin2)

# testing the direction of the motors and the actual code
fl_motor.forward()
rl_motor.backwards()
sleep(10)        
fl_motor.stop()
rl_motor.stop()
sleep(10)    
fl_motor.backwards()
rl_motor.forwards()
sleep(10)       
fl_motor.forward()
rl_motor.backwards()
sleep(10)
fl_motor.stop()
rl_motor.stop()