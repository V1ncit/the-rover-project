from hcsr04 import HCSR04 # Library for the ultrasonic sensors
from dcmotor import DCMotor         
from machine import Pin, PWM   
from time import sleep 
import time
import machine

# initialize pins
buzzer = machine.Pin(33, machine.Pin.OUT)
sensor_left = HCSR04(trigger_pin = 25, echo_pin = 26, echo_timeout_us = 10000)
sensor_right = HCSR04(trigger_pin = 17, echo_pin = 16, echo_timeout_us = 10000)

# initialize front left motor
fl_motor_pin1 = Pin(14, Pin.OUT)    
fl_motor_pin2 = Pin(27, Pin.OUT)     
fl_motor = DCMotor(fl_motor_pin1, fl_motor_pin2)

# initialize rear left motor
rl_motor_pin1 = Pin(13, Pin.OUT)
rl_motor_pin2 = Pin(32, Pin.OUT)
rl_motor = DCMotor(rl_motor_pin1, rl_motor_pin2)

# initialize front right motor
# fr_motor_pin1 = Pin(??, Pin.OUT)
# fr_motor_pin2 = Pin(??, Pin.OUT)
# fr_motor = DCMotor(fr_motor_pin1, fr_motor_pin2)

# initialize rear right motor
# rr_motor_pin1 = Pin(??, Pin.OUT)
# rr_motor_pin2 = Pin(??, Pin.OUT)
# rr_motor = DCMotor(rr_motor_pin1, rr_motor_pin2)

# set variables
left_motors_speed = 0
right_motors_speed = 0
distance_left = 0
distance_right = 0
total_distance = 0

# create lists to iron out avg distance
right_list = [20, 20, 20, 20, 20, 20, 20, 20, 20, 20]
left_list = [20, 20, 20, 20, 20, 20, 20, 20, 20, 20]

# set minimum distance to walls
min_dist = 10.0
err_dist = 5.0

# Function that sounds the buzzer t times
# TODO: Replace sleepfunction with time loops
def buzz(f):
    buzzer.value(0)
    time.sleep(f)
    buzzer.value(1)
    time.sleep(f)
    buzzer.value(0)

# function for getting the input from the sensors
def sensors():
    distance_left = int(sensor_left.distance_cm())
    distance_right = int(sensor_right.distance_cm())
    total_distance = distance_left + distance_right + 13
    return distance_left, distance_right, total_distance
    
while True:
    # calls the sensors function to get distances
    left, right, total = sensors()
    
    # manages the lists to even out the output
    left_list.append(left)
    left_list.pop(0)
    right_list.append(right)
    right_list.pop(0)
    
    # calculates the averages of the lists
    left_avg = sum(left_list) / len(left_list)
    right_avg = sum(right_list) / len(right_list)
    #print('Left:', left, 'cm, Right:', right, 'cm.')
    #print('Left:', left_avg, 'cm. Right:', right_avg, 'cm.')
    #print('Total:', total, 'cm.')
    
    # If statements to slow down motors if distance to walls is to short
    if right_avg < min_dist and right_avg > err_dist and right != 0:
        print('Slow down left motor')
        fl_motor.stop()
        buzz(0.1)
    elif left_avg < min_dist and left_avg > err_dist and left != 0:
        print('Slow down right motor')
        buzz(0.1)
    elif right_avg <= err_dist or left_avg <= err_dist and right != 0 and left != 0:
        buzz(0.05)
        print('someting wrooong')
    elif right == 0 and left == 0:
        buzz(1)
        print('Stop motors')
        fl_motor.stop()
        rl_motor.stop()
        # fr_motor.stop()
        # rr_motor.stop()
    else:
        print('FULL BLAST!!')
        fl_motor.forward()
        rl_motor.forward()
        # fr_motor.forward()
        # rr_motor.forward()
